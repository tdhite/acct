#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import acct.scripts

if __name__ == "__main__":
    acct.scripts.start()
