import pytest
import tempfile
import yaml


RAW_YAML = """
provider:
  profile:
    kwarg1: value1
    list1:
      - value0
      - value1
"""


@pytest.mark.asyncio
async def test_generate_key(hub):
    plugins = [p.__name__ for p in hub.crypto if p.__name__ != "init"]
    for plugin in plugins:
        assert await hub.crypto.init.generate_key(plugin)


@pytest.mark.asyncio
async def test_encrypt(hub):
    plugins = [p.__name__ for p in hub.crypto if p.__name__ != "init"]

    data = yaml.safe_load(RAW_YAML)

    for plugin in plugins:
        key = await hub.crypto.init.generate_key(plugin)
        encrypted = hub.crypto.fernet.encrypt(data, key)
        assert encrypted != data
        assert encrypted


@pytest.mark.asyncio
async def test_decrypt(hub):
    plugins = [p.__name__ for p in hub.crypto if p.__name__ != "init"]

    data = yaml.safe_load(RAW_YAML)

    for plugin in plugins:
        key = await hub.crypto.init.generate_key(plugin)
        encrypted = hub.crypto.fernet.encrypt(data, key)
        decrypted = hub.crypto.fernet.decrypt(encrypted, key)
        assert decrypted == data


@pytest.mark.asyncio
async def test_encrypt_file(hub):
    plugins = [p.__name__ for p in hub.crypto if p.__name__ != "init"]

    with tempfile.NamedTemporaryFile(delete=True) as cleartext_data:
        cleartext_data.write(RAW_YAML.encode())
        cleartext_data.flush()

        for plugin in plugins:
            with tempfile.NamedTemporaryFile(delete=True) as fh:
                key = await hub.crypto.init.encrypt_file(
                    crypto_plugin=plugin,
                    acct_file=cleartext_data.name,
                    output_file=fh.name,
                )
                assert key


@pytest.mark.asyncio
async def test_decrypt_file(hub):
    plugins = [p.__name__ for p in hub.crypto if p.__name__ != "init"]

    with tempfile.NamedTemporaryFile(delete=True) as cleartext_data:
        cleartext_data.write(RAW_YAML.encode())
        cleartext_data.flush()

        for plugin in plugins:
            with tempfile.NamedTemporaryFile(delete=True) as encoded_data:
                key = await hub.crypto.init.encrypt_file(
                    crypto_plugin=plugin,
                    acct_file=cleartext_data.name,
                    output_file=encoded_data.name,
                )
                data = await hub.crypto.init.decrypt_file(
                    crypto_plugin=plugin, acct_file=encoded_data.name, acct_key=key
                )

            assert data == yaml.safe_load(RAW_YAML)
